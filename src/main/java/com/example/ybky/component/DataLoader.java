package com.example.ybky.component;

import com.example.ybky.entity.Role;
import com.example.ybky.entity.User;
import com.example.ybky.entity.enums.RoleEnum;
import com.example.ybky.entity.enums.PermissionEnum;
import com.example.ybky.repository.RoleRepository;
import com.example.ybky.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    Set<PermissionEnum> permissionEnums =  Arrays.stream(PermissionEnum.values()).collect(Collectors.toSet());

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String modeType;

    @Override
    public void run(String... args) {
        if (Objects.equals("create", modeType)) {
            addRoles();
            addAdmin();
        }
    }
    private void addAdmin() {
        userRepository.save(
                User.builder()
                        .phone("+998940977766")
                        .password(passwordEncoder.encode("Bekjon@03"))
                        .role(new Role(2,"ADMIN","admin",permissionEnums,false))
                        .enabled(true)
                        .build()
        );
    }
    private void addRoles() {
        roleRepository.save(
                Role.builder()
                        .name(RoleEnum.USER.name())
                        .description("Client")
                        .build()
        );
        roleRepository.save(
                Role.builder()
                        .name(RoleEnum.ADMIN.name())
                        .description("Admin")
                        .permissions(setPermissionsOfAdmin())
                        .build()
        );
    }





    private Set<PermissionEnum> setPermissionOfUser() {
        return new HashSet<>(Arrays.stream(PermissionEnum.values()).toList());
    }

    private Set<PermissionEnum> setPermissionsOfAdmin() {
        return new HashSet<>(Arrays.stream(PermissionEnum.values()).toList());
    }

}