package com.example.ybky.controller;

import com.example.ybky.entity.User;
import com.example.ybky.payload.SignDTO;
import com.example.ybky.payload.SignInDTO;
import com.example.ybky.payload.TokenDTO;
import com.example.ybky.response.ApiResult;
import com.example.ybky.service.AuthService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@Slf4j
public class AuthControllerImpl implements AuthController{
    private final AuthService authService;

    @Override
    public ApiResult<SignDTO> signUp(@RequestBody @Valid SignDTO signDTO) {
        return authService.signUp(signDTO);
    }

    @Override
    public ApiResult<TokenDTO> signIn(SignInDTO signDTO) {
        return authService.signIn(signDTO);
    }

    @Override
    public ApiResult<?> delete(User user) {
        return authService.delete(user);
    }

    @Override
    public ApiResult<TokenDTO> refreshToken(String accessToken, String refreshToken) {
        return authService.refreshToken(accessToken,refreshToken);
    }

}
