package com.example.ybky.controller;

import com.example.ybky.aop.CurrentUser;
import com.example.ybky.entity.User;
import com.example.ybky.payload.BookingDTO;
import com.example.ybky.payload.LocalDateRange;
import com.example.ybky.payload.RoomDTO;
import com.example.ybky.response.ApiResult;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(path = RoomController.ROOM_CONTROLLER_BASE_PATH)
public interface RoomController {
    String ROOM_CONTROLLER_BASE_PATH = "/api/rooms";
    String ROOM_ADD_PATH = "/add";
    String ROOM_BOOK_PATH = "/book";
    String ROOM_AVAILABLE_PATH = "";
    String ROOM_SEARCH_PATH = "/search";

     @PostMapping(value = ROOM_ADD_PATH)
     ApiResult<RoomDTO> addRoom(@RequestBody RoomDTO roomDTO, @CurrentUser User user);

     @PostMapping(path = "/{roomName}"+ROOM_BOOK_PATH)
     ApiResult<String> bookRoom(@PathVariable("roomName") String roomName,
                              @RequestBody BookingDTO bookingDTO, @CurrentUser User user);

     @GetMapping(path = "/{roomName}"+ROOM_AVAILABLE_PATH)
     ApiResult<List<LocalDateRange>> getAvailablePeriods(@PathVariable("roomName") String roomName);

     @GetMapping()
     ApiResult<Page<RoomDTO>> getPages(@RequestParam(defaultValue = "1") int page,
                                       @RequestParam(defaultValue = "5") int pageSize,
                                       @RequestParam(value = "type",required = false) String type,
                                       @RequestParam(value = "search",required = false) String search
                                       );
}

