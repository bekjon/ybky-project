package com.example.ybky.controller;

import com.example.ybky.entity.User;
import com.example.ybky.payload.BookingDTO;
import com.example.ybky.payload.LocalDateRange;
import com.example.ybky.payload.RoomDTO;
import com.example.ybky.response.ApiResult;
import com.example.ybky.service.RoomService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@SecurityRequirement(name = "Authentication")
public class RoomControllerImpl implements RoomController  {
    private final RoomService roomService;
    @Override
    public ApiResult<RoomDTO> addRoom(RoomDTO roomDTO, User user) {
        return roomService.addRoom(roomDTO,user);
    }

    @Override
    public ApiResult<String> bookRoom(String roomName, BookingDTO bookingDTO, User user) {
        return roomService.bookRoom(roomName, bookingDTO,user);
    }

    @Override
    public ApiResult<List<LocalDateRange>> getAvailablePeriods(String roomName) {
        return roomService.getBookablePeriods(roomName);
    }

    @Override
    public ApiResult<Page<RoomDTO>> getPages(int page, int pageSize,String type,String search) {
        return  roomService.getPages(page,pageSize,type,search);
    }
}

