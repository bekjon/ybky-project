package com.example.ybky.entity;

import com.example.ybky.entity.enums.RoomType;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "room",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"name", "type"})
        })
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String name;
    private int capacity;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Booking> bookings = new ArrayList<>();

    @Column(name = "type", nullable = false)
    @Enumerated
    private RoomType type;
    public void addBooking(Booking booking) {
        bookings.add(booking);
        booking.setRoom(this);
    }
}
