package com.example.ybky.entity.enums;
import org.springframework.security.core.GrantedAuthority;
public enum PermissionEnum  implements GrantedAuthority{
    ADD_ROOM,DELETE_ROOM,EDIT_ROOM,BOOK_ROOM;
    @Override
    public String getAuthority() {
        return name();
    }
}
