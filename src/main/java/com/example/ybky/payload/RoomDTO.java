package com.example.ybky.payload;

import com.example.ybky.entity.enums.RoomType;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RoomDTO {
    @NotBlank(message = "REQUIRED_FIELD")
    String name;
    @NotNull(message = "REQUIRED_FIELD")
    RoomType type;
    @NotNull(message = "REQUIRED_FIELD")
    int capacity;
}
