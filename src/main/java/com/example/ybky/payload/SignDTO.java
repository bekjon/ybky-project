package com.example.ybky.payload;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
@AllArgsConstructor
public class SignDTO {
    @NotBlank
    private String phone;

    @NotBlank
    private String password;


    @Override
    public boolean equals(Object obj) {
        SignDTO other = (SignDTO) obj;

        if (!other.getPhone().equals(getPhone()))
            return false;

        return other.getPassword().equals(getPassword());
    }
}
