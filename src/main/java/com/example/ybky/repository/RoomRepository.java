package com.example.ybky.repository;

import com.example.ybky.entity.Room;
import com.example.ybky.entity.enums.RoomType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface RoomRepository extends JpaRepository<Room, Long> {
    boolean existsByName(String name);

    Page<Room> findByType(RoomType type, Pageable pageable);
    Page<Room> findByName(String search, Pageable pageable);
    Page<Room> findByTypeAndName(RoomType type, String name,Pageable pageable);

    boolean existsByNameAndType(String name, RoomType type);

    Optional<Room> findByName(String roomName);

}
