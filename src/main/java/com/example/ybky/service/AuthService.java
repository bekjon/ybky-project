package com.example.ybky.service;

import com.example.ybky.entity.User;
import com.example.ybky.payload.SignDTO;
import com.example.ybky.payload.SignInDTO;
import com.example.ybky.payload.TokenDTO;
import com.example.ybky.response.ApiResult;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;
import java.util.UUID;

public interface AuthService extends UserDetailsService {
    ApiResult<SignDTO> signUp(SignDTO signDTO);
    Optional<User> getUserById(UUID id);

    ApiResult<TokenDTO> signIn(SignInDTO signDTO);
    ApiResult<TokenDTO> refreshToken(String accessToken, String refreshToken);
    ApiResult<?> delete(User user);
}
