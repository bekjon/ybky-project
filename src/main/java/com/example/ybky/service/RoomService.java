package com.example.ybky.service;

import com.example.ybky.entity.User;
import com.example.ybky.payload.BookingDTO;
import com.example.ybky.payload.LocalDateRange;
import com.example.ybky.payload.RoomDTO;
import com.example.ybky.response.ApiResult;
import org.springframework.data.domain.Page;

import java.util.List;

public interface RoomService {

    ApiResult<String> bookRoom(String roomName, BookingDTO bookingDTO, User user);

    ApiResult<List<LocalDateRange>> getBookablePeriods(String roomName);

    ApiResult<RoomDTO> addRoom(RoomDTO roomDTO, User user);

    ApiResult<Page<RoomDTO>> getPages(int page, int pageSize,String type,String search);



//
//
//    //checking available room for given id
//
//    public ApiResult<Long> bookRoom(Long roomId, BookingDTO bookingDTO, User user) {
//        Room room = roomRepository.findById(roomId)
//                .orElseThrow(
//                        () -> RestException.restThrow(String.format("room with id %s not found", roomId), HttpStatus.NOT_FOUND));
//
//        if (!isRoomAvailable(room, bookingDTO.getStartDate(), bookingDTO.getEndDate())) {
//            throw RestException.restThrow("Room is not available at this period",HttpStatus.BAD_REQUEST);
//        }
//
//        Booking booking = Booking.builder()
//                .startDate(bookingDTO.getStartDate())
//                .endDate(bookingDTO.getEndDate())
//                .residentName(user.getUsername())
//                .build();
//        room.addBooking(booking);
//        roomRepository.save(room);
//
//        return ApiResult.successResponse(roomId);
//    }
//
//    //getting bookable period of all time of the room
//    public ApiResult<List<LocalDateRange>> getBookablePeriods(Long roomId) {
//        Room room = roomRepository.findById(roomId)
//                .orElseThrow(() -> RestException.restThrow(
//                        String.format("Room with id %s not found", roomId),
//                        HttpStatus.NOT_FOUND
//                ));
//
//        List<Booking> bookings = room.getBookings();
//        List<LocalDateRange> bookablePeriods = new ArrayList<>();
//
//        LocalDateTime currentDate = LocalDateTime.now();
//
//        LocalDateTime previousEndTime = currentDate;
//        LocalDateTime lastDayOfYear = Year.now().atMonth(12).atEndOfMonth().atTime(23, 59, 59);
//
//
//        // Sort the bookings by start date in ascending order
//        bookings.sort(Comparator.comparing(Booking::getStartDate));
//
//        for (Booking booking : bookings) {
//            LocalDateTime start = booking.getStartDate();
//            LocalDateTime end = booking.getEndDate();
//
//            if (start.isAfter(previousEndTime)) {
//                //1 hour for getting the room ready for new residence
//                bookablePeriods.add(new LocalDateRange(previousEndTime, start.minusHours(1)));
//            }
//            //1 hour for getting the room ready for new residence
//            previousEndTime = end.plusHours(1);
//        }
//
//        // Add the final bookable period from the last booking's end time until last day of this current year
//        if (!previousEndTime.isEqual(currentDate)) {
//            bookablePeriods.add(new LocalDateRange(previousEndTime,lastDayOfYear ));
//        }
//
//        return ApiResult.successResponse(bookablePeriods);
//    }
//    //adding room
//



//    //mapping

//
}