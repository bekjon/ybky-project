package com.example.ybky.service;

import com.example.ybky.entity.Booking;
import com.example.ybky.entity.Room;
import com.example.ybky.entity.User;
import com.example.ybky.entity.enums.RoomType;
import com.example.ybky.exception.RestException;
import com.example.ybky.payload.BookingDTO;
import com.example.ybky.payload.LocalDateRange;
import com.example.ybky.payload.RoomDTO;
import com.example.ybky.repository.RoomRepository;
import com.example.ybky.response.ApiResult;
import com.example.ybky.util.RestConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.Year;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService{
    private final RoomRepository roomRepository;
    @Override
    public ApiResult<String> bookRoom(String roomName, BookingDTO bookingDTO, User user) {

        Room room = roomRepository.findByName(roomName)
                .orElseThrow(
                        () -> RestException.restThrow(String.format("room with id %s not found", roomName), HttpStatus.NOT_FOUND));

        if (!isRoomAvailable(room, bookingDTO.getStartDate(), bookingDTO.getEndDate())) {
            throw RestException.restThrow("Room is not available at this period",HttpStatus.BAD_REQUEST);
        }

        Booking booking = Booking.builder()
                .startDate(bookingDTO.getStartDate())
                .endDate(bookingDTO.getEndDate())
                .residentName(user.getUsername())
                .build();
        room.addBooking(booking);
        roomRepository.save(room);

        return ApiResult.successResponse(roomName);
    }

    //getting bookable period of all time of the room
    public ApiResult<List<LocalDateRange>> getBookablePeriods(String roomName) {
        Room room = roomRepository.findByName(roomName)
                .orElseThrow(() -> RestException.restThrow(
                        String.format("Room with name %s not found", roomName),
                        HttpStatus.NOT_FOUND
                ));

        List<Booking> bookings = room.getBookings();
        List<LocalDateRange> bookablePeriods = new ArrayList<>();

        LocalDateTime currentDate = LocalDateTime.now();

        LocalDateTime previousEndTime = currentDate;
        LocalDateTime lastDayOfYear = Year.now().atMonth(12).atEndOfMonth().atTime(23, 59, 59);


        // Sort the bookings by start date in ascending order
        bookings.sort(Comparator.comparing(Booking::getStartDate));

        for (Booking booking : bookings) {
            LocalDateTime start = booking.getStartDate();
            LocalDateTime end = booking.getEndDate();

            if (start.isAfter(previousEndTime)) {
                //1 hour for getting the room ready for new residence
                bookablePeriods.add(new LocalDateRange(previousEndTime, start.minusHours(1)));
            }
            //1 hour for getting the room ready for new residence
            previousEndTime = end.plusHours(1);
        }

        // Add the final bookable period from the last booking's end time until last day of this current year
        if (!previousEndTime.isEqual(currentDate)) {
            bookablePeriods.add(new LocalDateRange(previousEndTime,lastDayOfYear ));
        }

        return ApiResult.successResponse(bookablePeriods);
    }

    @Override
    public ApiResult<RoomDTO> addRoom(RoomDTO roomDTO, User user) {
        if(!user.getRole().getName().equals("ADMIN")){
            throw new RestException("ACCESS_DENIED_FOR_THIS_METHOD",HttpStatus.CONFLICT);
        }
        if (roomRepository.existsByNameAndType(roomDTO.getName(),roomDTO.getType())) {
            throw new RestException(RestConstants.ALREADY_EXISTED,HttpStatus.CONFLICT);
        }
        Room room = Room.builder()
                .name(roomDTO.getName())
                .type(roomDTO.getType())
                .capacity(roomDTO.getCapacity())
                .build();
        roomRepository.save(room);
        return ApiResult.successResponse(roomDTO);
    }

    @Override
    public ApiResult<Page<RoomDTO>> getPages(int page, int pageSize,String type,String search) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        if(type!=null && search!=null){
            Page<Room> roomsByTypeAndName = roomRepository.findByTypeAndName(RoomType.valueOf(type), search,pageRequest);
            return ApiResult.successResponse(roomsByTypeAndName.map(this::mapRoomToRoomDTO));
        }

       else if (search!=null) {
            Page<Room> roomsByName = roomRepository.findByName(search,pageRequest);
            return ApiResult.successResponse(roomsByName.map(this::mapRoomToRoomDTO));
        }
       else if (type!=null) {
            Page<Room> roomsByType = roomRepository.findByType(RoomType.valueOf(type),pageRequest);
            return ApiResult.successResponse(roomsByType.map(this::mapRoomToRoomDTO));
        }
       else {
            Page<Room> rooms = roomRepository.findAll(pageRequest);
            return ApiResult.successResponse(rooms.map(this::mapRoomToRoomDTO));
        }
    }

    private boolean isRoomAvailable(Room room, LocalDateTime startDate, LocalDateTime endDate) {
        if (!isValidPeriod(startDate, endDate)) {
            throw new RestException("Invalid booking period", HttpStatus.BAD_REQUEST);
        }

        return room.getBookings().stream()
                .noneMatch(booking -> (booking.getStartDate().isBefore(endDate) && booking.getEndDate().isAfter(startDate) && !booking.getEndDate().isEqual(startDate))
                        || (booking.getStartDate().isEqual(startDate) || booking.getEndDate().isEqual(endDate) || booking.getEndDate().isEqual(startDate)));
    }

    //checking given period is valid or not
    private boolean isValidPeriod(LocalDateTime startDate, LocalDateTime endDate) {
        LocalDateTime currentDate = LocalDateTime.now();
        return !startDate.isBefore(currentDate) && !startDate.isAfter(endDate);
    }
    //mapping room object to roomDTO
    private RoomDTO mapRoomToRoomDTO(Room room){
        return RoomDTO.builder()
                .name(room.getName())
                .type(room.getType())
                .build();
    }
}
