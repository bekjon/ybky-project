package com.example.ybky.service;

import com.example.ybky.error.ErrorData;
import com.example.ybky.payload.SignDTO;
import com.example.ybky.repository.UserRepository;
import com.example.ybky.util.StringHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@RequiredArgsConstructor
@Component
public class ValidatorService {

    private final UserRepository userRepository;

    public List<ErrorData> validateUser(SignDTO user) {
        List<ErrorData> errors = new ArrayList<>();
        if (userRepository.existsByPhone(user.getPhone()))
            addError("PhoneNumber", "NOT_UNIQUE", errors);
        if (!StringHelper.isValidPassword(user.getPassword()))
            addError("Password", "Must contains at least 1 upper, 1 lower and 1 numeric character", errors);
        return errors;
    }



    private void addError(String fieldName, String error, List<ErrorData> list) {
        list.add(ErrorData.builder()
                .errorMsg(error)
                .fieldName(fieldName)
                .build());
    }


}
