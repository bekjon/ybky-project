package com.example.ybky.util;

import com.example.ybky.controller.AuthController;
import com.fasterxml.jackson.databind.ObjectMapper;
public interface RestConstants {
    ObjectMapper objectMapper = new ObjectMapper();

    String AUTHENTICATION_HEADER = "Authorization";

    String[] OPEN_PAGES = {
            "/*",
            "/api/load/open-loads",
            "/swagger-ui/index.html",
            AuthController.AUTH_CONTROLLER_BASE_PATH + "/**",
            RestConstants.BASE_PATH + "/**"
    };
    String BASE_PATH = "/api/v1/";

    String ALREADY_EXISTED = "already existed";
}
