import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import RoomForm from "./component/RoomForm";
import SignInForm from "./component/SignInForm";
import SignUpForm from "./component/SignUpForm";
import RoomsPage from "./component/RoomsPage";
import BookRoomPage from "./component/BookRoomPage";

const App: React.FC = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<SignUpForm />} />
        <Route path="/signin" element={<SignInForm />} />
        <Route path="/roomform" element={<RoomForm />} />
        <Route path="/rooms" element={<RoomsPage />} />
        <Route path="/rooms/:roomName" element={<BookRoomPage />} />
      </Routes>
    </Router>
  );
};

export default App;
