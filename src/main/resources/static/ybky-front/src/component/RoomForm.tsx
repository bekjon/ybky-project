import React, { useState } from "react";
import axios from "axios";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  Input,
  Alert,
  AlertIcon,
  Select,
} from "@chakra-ui/react";

const RoomForm = () => {
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [capacity, setCapacity] = useState("");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    // Create the payload object
    const payload = {
      name,
      type,
      capacity: parseInt(capacity, 10),
    };

    try {
      // Retrieve the token from local storage
      const token = localStorage.getItem("token");

      // Set the request headers with the token
      const headers = {
        Authorization: `Bearer ${token}`,
      };

      // Send the POST request to the backend API with the token
      await axios.post("http://localhost:8090/api/rooms/add", payload, {
        headers,
      });

      // Clear form inputs
      setName("");
      setType("");
      setCapacity("");

      // Show success message
      setSuccess("Room added successfully!");

      // Clear error message
      setError("");
    } catch (error) {
      // Display the error message to the user
      setError("Error adding room. Please try again.");
    }
  };

  return (
    <Box>
      {error && (
        <Alert status="error" marginBottom="1rem">
          <AlertIcon />
          {error}
        </Alert>
      )}
      {success && (
        <Alert status="success" marginBottom="1rem">
          <AlertIcon />
          {success}
        </Alert>
      )}
      <form onSubmit={handleSubmit}>
        <FormControl>
          <FormLabel htmlFor="name">Name</FormLabel>
          <Input
            id="name"
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </FormControl>

        <FormControl>
          <FormLabel htmlFor="type">Type</FormLabel>
          <Select
            id="type"
            value={type}
            onChange={(e) => setType(e.target.value)}
          >
            <option value=""> </option>
            <option value="focus">Focus</option>
            <option value="team">Team</option>
            <option value="conference">Conference</option>
          </Select>
        </FormControl>

        <FormControl>
          <FormLabel htmlFor="capacity">Capacity</FormLabel>
          <Input
            id="capacity"
            type="number"
            value={capacity}
            onChange={(e) => setCapacity(e.target.value)}
          />
        </FormControl>

        <Button type="submit">Submit</Button>
      </form>
    </Box>
  );
};

export default RoomForm;
