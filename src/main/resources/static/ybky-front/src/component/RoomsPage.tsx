import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Box,
  Select,
  Input,
  VStack,
  Button,
  SimpleGrid,
  Text,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

const RoomsPage = () => {
  const [rooms, setRooms] = useState([]);
  const [typeFilter, setTypeFilter] = useState("");
  const [searchFilter, setSearchFilter] = useState("");
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(4);
  const navigate = useNavigate();

  const fetchRooms = async () => {
    try {
      const token = localStorage.getItem("token");

      const params = {
        page,
        pageSize,
        ...(typeFilter && { type: typeFilter }), // Include type parameter if it is not empty
        ...(searchFilter && { search: searchFilter }), // Include search parameter if it is not empty
      };

      const response = await axios.get("http://localhost:8090/api/rooms", {
        params,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      setRooms(response.data.data.content);
    } catch (error) {
      console.error("Error fetching rooms:", error);
    }
  };

  useEffect(() => {
    fetchRooms();
  }, [page, pageSize, typeFilter, searchFilter]);

  useEffect(() => {
    setPage(1); // Reset page to 1 when type filter changes
  }, [typeFilter]);

  const handleSearch = () => {
    setPage(1); // Reset page to 1 when searching
    fetchRooms();
  };

  const handleSearchChange = (e) => {
    setSearchFilter(e.target.value);
  };

  const handlePageChange = (newPage) => {
    setPage(newPage);
  };

  const handleBookRoom = (roomName) => {
    navigate(`/rooms/${roomName}`);
  };

  return (
    <Box>
      <VStack spacing={4} marginBottom={4}>
        <Select
          value={typeFilter}
          onChange={(e) => setTypeFilter(e.target.value)}
          placeholder="Select type"
        >
          <option value="">All</option>
          <option value="focus">Focus</option>
          <option value="team">Team</option>
          <option value="conference">Conference</option>
        </Select>
        <Input
          type="text"
          value={searchFilter}
          onChange={handleSearchChange}
          placeholder="Search"
        />
        <Button onClick={handleSearch}>Search</Button>
      </VStack>

      {rooms.length > 0 ? (
        <SimpleGrid columns={2} spacing={4}>
          {rooms.map((room) => (
            <Card key={room.name}>
              <CardHeader>
                <Text fontSize="xl" fontWeight="bold">
                  {room.name}
                </Text>
              </CardHeader>
              <CardBody>
                <Text>Type: {room.type}</Text>
                <Text>Capacity: {room.capacity}</Text>
              </CardBody>
              <CardFooter>
                <Button
                  colorScheme="blue"
                  onClick={() => handleBookRoom(room.name)}
                >
                  Book Room
                </Button>
              </CardFooter>
            </Card>
          ))}
        </SimpleGrid>
      ) : (
        <div>No rooms found</div>
      )}

      <Box>
        <Button
          onClick={() => handlePageChange(page - 1)}
          disabled={page === 1}
        >
          Previous Page
        </Button>
        <Button onClick={() => handlePageChange(page + 1)}>Next Page</Button>
      </Box>
    </Box>
  );
};

export default RoomsPage;
